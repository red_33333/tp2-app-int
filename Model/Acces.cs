﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1porteLock.Model{
    public class Acces : INotifyPropertyChanged{

        private string numEmploye;
        private string noLocal;
        private string jour;
        private string heureDebut;
        private string heureFin;

        public Acces(){}

        public Acces(string nnumEmploye, string nnoCarte, string njour, string nheureDebut, string nheureFin){

            numEmploye = nnumEmploye;
            noLocal = nnoCarte;
            jour = njour;
            heureDebut = nheureDebut;
            heureFin = nheureFin;

        }

        public string NumEmploye{
            get { return numEmploye; }
            set { numEmploye = value; OnPropertyChanged(NumEmploye); }
        }

        public string NoLocal{
            get { return noLocal; }
            set { noLocal = value; OnPropertyChanged(NoLocal); }
        }

        public string Jour{
            get { return jour; }
            set { jour = value; OnPropertyChanged(Jour); }
        }

        public string HeureDebut{
            get { return heureDebut; }
            set { heureDebut = value; OnPropertyChanged(HeureDebut); }
        }

        public string HeureFin{
            get { return heureFin; }
            set { heureFin = value; OnPropertyChanged(HeureFin); }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string nprop){
            PropertyChangedEventHandler propHandler = PropertyChanged;
            if (propHandler != null){
                propHandler(this, new PropertyChangedEventArgs(nprop));
            }

        }
    }

}
