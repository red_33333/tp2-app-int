﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TP1porteLock.Command;
using TP1porteLock.Model;




namespace TP1porteLock.ViewModel{
    public class LocalViewModel : INotifyPropertyChanged{
        private Room _room;
        private ObservableCollection<Room> _rooms;
        private ICommand _SubmitCommand;

        public LocalViewModel(){

            Room = new Room();
            _rooms = new ObservableCollection<Room>();

            //HARDCODED ENTRIES
            _rooms.Add(new Room(RandomNumber(100, 200).ToString(), "Description local A"));
            _rooms.Add(new Room(RandomNumber(200, 300).ToString(), "Description local B"));
            _rooms.Add(new Room(RandomNumber(300, 400).ToString(), "Description local C"));

        }


        public Room Room{
            get { return _room; }
            set { _room = value; NotifyPropertyChanged("Room"); }
        }


        public int RandomNumber(int min, int max){
            Random random = new Random();
            return random.Next(min, max);
        }


        public ObservableCollection<Room> Rooms{
            get{
                return _rooms;
            }
            set{
                _rooms = value;
                NotifyPropertyChanged("Persons");
            }
        }



        public ICommand SubmitCommand{
            get{

                if (_SubmitCommand == null){
                    _SubmitCommand = new RelayCommand(SubmitExecute, CanSubmitExecute, false);
                }

                return _SubmitCommand;

            }
        }



        public void DeleteRoom(object parameter){
            Rooms.Remove(Room);
        }



        private void SubmitExecute(object parameter){
            Rooms.Add(Room);
        }



        private bool CanSubmitExecute(object parameter){

            if (Room == null){
                return false;
            }

            if (string.IsNullOrEmpty(Room.Numero) || string.IsNullOrEmpty(Room.Description)){
                return false;
            }

            return true;

        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propName)
        {

            if (PropertyChanged != null){
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }

        }




    }
}
