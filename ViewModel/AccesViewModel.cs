﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TP1porteLock.Command;
using TP1porteLock.Model;




namespace TP1porteLock.ViewModel{
    public class AccesViewModel : INotifyPropertyChanged{

        private Acces _acces;
        private ObservableCollection<Acces> _access;
        private ICommand _SubmitCommand;

        public AccesViewModel(){

            _acces = new Acces();
            _access = new ObservableCollection<Acces>();

      
            //HARDCODED ENTRIES
            _access.Add(new Acces("101", "501", "Lundi", "12h00", "13h00"));
            _access.Add(new Acces("102", "502", "Mardi", "14h00", "15h00"));
            _access.Add(new Acces("103", "503", "Mercredi", "16h00", "17h00"));


        }


        public Acces Acces{
            get { return _acces; }
            set { _acces = value; NotifyPropertyChanged("Acces"); }
        }
        

        public ObservableCollection<Acces> Access{
            get{ return _access; }
            set{ _access = value; NotifyPropertyChanged("Access"); }
        }



        public ICommand SubmitCommand{
            get{

                if (_SubmitCommand == null){
                    _SubmitCommand = new RelayCommand(SubmitExecute, CanSubmitExecute, false);
                }

                return _SubmitCommand;

            }
        }



        public void DeleteRoom(object parameter){
            Access.Remove(Acces);
        }



        private void SubmitExecute(object parameter){
            Access.Add(Acces);
        }



        private bool CanSubmitExecute(object parameter){

            if (Acces == null){
                return false;
            }
            if (string.IsNullOrEmpty(Acces.HeureDebut) || string.IsNullOrEmpty(Acces.HeureFin) || string.IsNullOrEmpty(Acces.Jour)){
                return false;
            }

            return true;

        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propName){

            if (PropertyChanged != null){
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }

        }




    }
}