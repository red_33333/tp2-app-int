﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TP1porteLock.Command;
using TP1porteLock.Model;




namespace TP1porteLock.ViewModel{
    public class PersonViewModel : INotifyPropertyChanged{

        private Person _person;
        private ObservableCollection<Person> _persons;
        private ICommand _SubmitCommand;
        
        public PersonViewModel(){

            Person = new Person();
            Persons = new ObservableCollection<Person>();


            //HARDECODED ENTRIES
            Persons.Add(new Person("Jacques", "Durant", "001", "C101"));
            Persons.Add(new Person("Benoit", "Dupont", "002", "C102"));
            Persons.Add(new Person("Martin", "Levesque", "003", "C103"));

        }


        public Person Person{
            get { return _person; }
            set { _person = value; NotifyPropertyChanged("Person"); }
        }





        public ObservableCollection<Person> Persons{
            get{
                return _persons;
            }
            set{
                _persons = value;
                NotifyPropertyChanged("Persons");
            }
        }



        public ICommand SubmitCommand{
            get{

                if (_SubmitCommand == null){
                    _SubmitCommand = new RelayCommand(SubmitExecute, CanSubmitExecute, false);
                }

                return _SubmitCommand;

            }
        }



        private void SubmitExecute(object parameter) {
            Persons.Add(Person);
        }


        private bool CanSubmitExecute(object parameter){

            if(Person == null){
                return false;
            }

            if (string.IsNullOrEmpty(Person.FName) || string.IsNullOrEmpty(Person.LName)){
                return false;                 
            }
            
            return true;

        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propName){

            if (PropertyChanged != null){
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }

        }




    }
}
